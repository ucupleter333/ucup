<?php

namespace App\Http\Controllers;

use App\Models\ys;
use Illuminate\Http\Request;

class YsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allys = ys::all();
        return view('ys-semua', compact('allys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ys-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ys::create([
            'nama'=>$request->nama,
            'alamat'=>$request->alamat,
            'kelas'=>$request->kelas,
        ]);

        return redirect('tampil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ys  $ys
     * @return \Illuminate\Http\Response
     */
    public function show(ys $id)
    {
        $yssatu = ys::find($id);
        return view('ys-satu', compact('yssatu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ys  $datays
     * @return \Illuminate\Http\Response
     */
    public function edit(ys $ys, $id)
    {
        $ubah = ys::findorfail($id);
        return view('ys-edit',compact('ubah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ys  $ys
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ubah = ys::findorfail($id);
        $ubah->update($request->all());

        return redirect('tampil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ys  $ys
     * @return \Illuminate\Http\Response
     */
    public function destroy(ys $ys, $id)
    {
        $ubah = ys::findorfail($id);
        $ubah->delete();

        return back();
    }
}
