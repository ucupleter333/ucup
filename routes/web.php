<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\YsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('conten');
});

Route::get('tampil',[YsController::class, 'index']);

Route::get('tampil/{id}',[YsController::class, 'show']);

Route::get('buatdata',[YsController::class, 'create']);
Route::post('simpandata',[YsController::class, 'store']);

Route::get('editdata/{id}',[YsController::class, 'edit']);
Route::post('update/{id}',[YsController::class, 'update']);

Route::get('delete/{id}',[YsController::class, 'destroy']);