<table>
    <tr>
        <th>ID</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Kelas</th>
        <th>Opsi</th>
    </tr>
    @foreach ($allys as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td><a href="{{ url('tampil', $item->id) }}">{{ $item->nama }}</a></td>
            <td>{{ $item->Alamat }}</td>
            <td>{{ $item->kelas }}</td>
            <td>
                <a href="{{ url('editdata', $item->id) }}">Edit</a>
                <a href="{{url('delete', $item->id)}}">Hapus</a>
            </td>
        </tr>
    @endforeach
</table>
<button><a href="buatdata">Tambah</a></button>